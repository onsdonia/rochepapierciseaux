CC=gcc
CFLAGS=-std=c99 -Wall

# additional flags for gcov
TESTFLAGS=-fprofile-arcs -ftest-coverage

app : jeu.h jeu.c app.c
	# build the app
	$(CC) $(CFLAGS) -o app app.c jeu.c

	# run the app
	./app

testcomparaison: testcomparaison.c jeu.h jeu.c
	# build the comparaison test
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testcomparaison testcomparaison.c jeu.c

	# run the test, which will generate testcomparaison.gcna and testcomparaison.gcno
	./testcomparaison

	# compute how test is covering testcomparaison.c
	gcov -c -p testcomparaison
